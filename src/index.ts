require("dotenv").config();
import { config } from "./config";
import { createApp } from "./createApp";

const server = createApp(config);

server.listen(config.port, () => {
  console.log(`Server Running at https://localhost:${config.port}`);
});
