export interface Config {
  databaseUrl?: string;
  port: number;
}

export const config: Config = {
  databaseUrl: process.env.DATABASE_URL,
  port: 3000,
};
