import { Application } from "express";
import request from "supertest";
import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";
import { createApp } from "./createApp";

let app: Application;
let mongod: MongoMemoryServer;

// setup the test dependencies
beforeAll(async () => {
  mongod = await MongoMemoryServer.create();

  const uri = mongod.getUri();

  const testConfig = {
    databaseUrl: uri,
    port: 3000,
  };

  app = createApp(testConfig);
});

afterAll(async () => {
  await mongoose.connection.close();
  await mongod.stop();
});

describe("GET /activities", function () {
  it("responds with a list of activities", function (done) {
    request(app)
      .get("/activities")
      .set("Accept", "application/json")
      .expect(200, done);
  });
});
